export const state = () => ({
  bookData: {
    id: null,
    isbn: '',
    title: null,
    authors:[],
    editor: '',
    category: [],
    year: null,
    pages: null,
    position: null,
    story: null,
  }
})

export const mutations = {
  changeBookData(state, payload) {
    const {key, value} = payload;
    state.bookData[key] = value;
  }
}
